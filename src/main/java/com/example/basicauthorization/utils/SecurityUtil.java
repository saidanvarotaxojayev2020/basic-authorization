package com.example.basicauthorization.utils;

import com.example.basicauthorization.models.User;
import com.example.basicauthorization.security.MyUserPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class SecurityUtil {

    public User getCurrentUser() {
        MyUserPrincipal securityUser = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return securityUser.getUser();
    }

}
