package com.example.basicauthorization.enums;

public enum Status {
    ACTIVE, INACTIVE;
}
