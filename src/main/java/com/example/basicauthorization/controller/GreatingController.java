package com.example.basicauthorization.controller;

import com.example.basicauthorization.dtos.UserDto;
import com.example.basicauthorization.enums.Status;
import com.example.basicauthorization.models.User;
import com.example.basicauthorization.repository.UserRepo;
import com.example.basicauthorization.utils.SecurityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class GreatingController {


    private final UserRepo userRepo;
    private final SecurityUtil securityUtil;
    private final PasswordEncoder passwordEncoder;

    @GetMapping("/great")
    public String getGreating() {
        return "Success";
    }

    @GetMapping("/user")
    public User getUser() {
        return securityUtil.getCurrentUser();
    }

    @PostMapping("/add")
    public User addUser(@RequestBody UserDto userDto) {
        User user = new User(userDto.getUsername(), passwordEncoder.encode(userDto.getPassword()), Status.ACTIVE);
        return userRepo.save(user);
    }

}
